import { defineStore } from "pinia";
import axios from "axios";

export const userUserStore = defineStore("users", {
  state: () => ({ users: [] }),
  getters: {
    usersData: (state) => state.users,
  },
  actions: {
    async getAllUsers() {
      const result = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );

      this.users = result.data;
    },
  },
});
