import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/questionOne.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "questionOne",
    component: HomeView,
  },
  {
    path: "/que-two",
    name: "questionTwo",
    component: () => import("../views/questionTwo.vue"),
  },
  {
    path: "/que-three",
    name: "questionThree",
    component: () => import("../views/questionThree.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
